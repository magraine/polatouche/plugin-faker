<?php
namespace Polatouche\Plugin\Faker;

use Pimple\Container;
use Polatouche\Plugin\PluginProviderInterface;

class FakerPlugin implements PluginProviderInterface {
	/**
	 * Registers services on the given app.
	 *
	 * @param Container $app An Application instance.
	 */
	public function register(Container $app) {

	}
}
