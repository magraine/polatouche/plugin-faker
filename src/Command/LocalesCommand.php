<?php

namespace Polatouche\Plugin\Faker\Command;

use Polatouche\Provider\Console\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;



class LocalesCommand extends Command {

	protected function configure(){
		// triste : le translator est pas dispo encore dans cette fonction.
		$this->setName("faker:locales")
			->setDescription('List the locales that can be used with Faker')
			->addOption('with-real-text', 'w', InputOption::VALUE_NONE, 'List locales with real text capability');
	}

	protected function execute(InputInterface $input, OutputInterface $output) {
		$this->io = $this->getIO($input, $output);
		$this->io->title($this->trans('faker.command.locales.title'));
		$withRealText = $input->getOption('with-real-text');
		$locales = self::getFakerLocales($withRealText);
		if ($withRealText) {
			$this->io->text($this->trans('faker.command.locales.list.realtext'));
		} else {
			$this->io->text($this->trans('faker.command.locales.list.all'));
		}

		$this->io->columns($locales, 6, true);
	}


	/**
	 * Liste des locales possibles de Faker
	 * @return array
	 */
	static public function getFakerLocales($withRealText = false) {
		$class = new \ReflectionClass('\Faker\Factory');
		$dir = dirname($class->getFilename());
		$dir = $dir . DIRECTORY_SEPARATOR . 'Provider';
		if ($withRealText) {
			$files = (new Finder())->files()->in($dir)->name('Text.php')->depth('== 1');
			$directories = [];
			foreach ($files as $file) {
				$directories[] = new \SplFileInfo(dirname($file->getPathname()));
			}
		} else {
			$directories = (new Finder())->directories()->in($dir);
		}
		$liste = [];
		foreach($directories as $d) {
			$liste[] = $d->getFilename();
		}
		return $liste;
	}
}
