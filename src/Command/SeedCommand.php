<?php

namespace Polatouche\Plugin\Faker\Command;

use Polatouche\Plugin\Faker\Seeder\Base as Seeder;
use Polatouche\Provider\Console\Command;
use Polatouche\Provider\Spip\Loader;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;



class SeedCommand extends Command {

	protected $input;
	protected $output;
	protected $default_locale = 'fr_FR';

	protected $locale;
	protected $count = [];

	protected function configure(){
		// triste : le translator est pas dispo encore dans cette fonction.
		$this->setName("faker:seed")
			->setDescription('Seed a SPIP website with fake content')
			->addOption('locale', null, InputOption::VALUE_OPTIONAL, 'Locale to use ?');
	}

	protected function execute(InputInterface $input, OutputInterface $output) {
		$this->input = $input;
		$this->ouput = $output;
		$this->io = $this->getIO($input, $output);

		$this->io->title($this->trans('faker.command.seed.title'));

		/** @var Loader $spip */
		$spip = $this->getService('spip.loader');
		$spip->load();

		$this->locale = $this->askLocale($input, $output);

		// inits diverses [TODO : options / questions ]
		$this->count['rubriques'] = 1;
		$this->count['articles'] = 10;
		$this->count['auteurs'] = 3;
		$this->count['mots'] = 8;

		$seeder = new Seeder();
		$seeder->seed($this->locale, $this->count);
	}


	/**
	 * Retourne la langue à utiliser. La demande si non indiquées dans les options.
	 */
	protected function askLocale() {
		$locales = LocalesCommand::getFakerLocales();
		$locale = $this->input->getOption('locale');
		if (!$locale) {
			$locale = $this->io->ask($this->trans('faker.command.seed.option.locale.question'), $this->default_locale, function ($answer) use ($locales) {
				if (!is_string($answer) || !in_array($answer, $locales)) {
					throw new \RuntimeException(
						$this->trans('faker.error.locale.unknown', ['%locale%' => $answer])
					);
				}
				return $answer;
			});
		}
		$this->validateLocale($locale);
		$this->io->text($this->trans('faker.command.seed.option.locale.answer') . ' <info>' . $locale . '</info>');
		return $locale;
	}

	public function validateLocale($locale) {
		$locales = LocalesCommand::getFakerLocales();
		if (!$locale or !in_array($locale, $locales)) {
			throw new \RuntimeException(
				$this->trans('faker.error.locale.unknown', ['%locale%' => $locale])
			);
		}
		return true;
	}
}
