<?php

return [
	'faker' => [
		'error.locale.unknown' => 'La locale %locale% n’existe pas dans Faker',
		'command' => [
			'locales' => [
				'title' => 'Liste les locales utilisables dans Faker',
				'list.all' => 'Liste les locales :',
				'list.realtext' => 'Liste les locales avec faux textes complets :',
			],
			'seed' => [
				'title' => 'Peupler un site',
				'option.locale.question' => 'Quelle locale ?',
				'option.locale.answer' => 'Langue choisie :',
			],
		],
	],
];
