<?php

namespace Polatouche\Plugin\Faker\Seeder;

use Faker\Factory as Faker;
use Faker\Generator;

class Base {

	/** @var Generator $faker */
	protected $faker;
	protected $locale;
	protected $lang;
	protected $count = [];

	public function __construct() {}

	/**
	 * Remplir le site SPIP avec la locale et le nombre d’élémets indiqués.
	 *
	 * @param string $locale
	 * @param array $count
	 */
	public function seed($locale, $count = []) {
		$this->faker = Faker::create($locale);
		$this->lang = substr($locale, 0, 2);

		$this->count = $count + [
			'rubriques' => 1,
			'articles' => 10,
			'auteurs' => 3,
			'mots' => 8,
		];

		include_spip('action/editer_objet');
		include_spip('action/editer_liens');
		include_spip('inc/autoriser');

		$this->seedRubriques();
	}

	/** Générer des rubriques */
	public function seedRubriques() {
		for ($i = 0; $i < $this->count['rubriques']; $i++ ) {
			$this->seedRubrique();
		}
	}


	/** Générer une rubrique */
	public function seedRubrique() {
		$data = [
			'titre' => $this->generateTitle(20, 40),
			'texte' => $this->generateText(),
			'statut' => 'publie',
			'lang' => $this->lang,
		];
		if ($this->faker->numberBetween(0, 100) < 80) {
			$data['descriptif'] = $this->generateParagraph();
		}

		autoriser_exception('creer', 'rubrique', '', true);
		$id_rubrique = objet_inserer('rubrique', 0, $data);
		autoriser_exception('creer', 'rubrique', '', false);

		$auteurs = $this->seedAuteurs();
		$articles = $this->seedArticles($id_rubrique);
		$this->seedLinks(
			['auteurs' => $auteurs],
			['articles' => $articles]
		);
	}

	/** Générer des auteurs */
	public function seedAuteurs() {
		$auteurs = [];
		for ($i = 0; $i < $this->count['auteurs']; $i++) {
			if ($id_auteur = $this->seedAuteur()) {
				$auteurs[] = $id_auteur;
			}
		}
		return $auteurs;
	}

	/** Générer un auteur */
	function seedAuteur() {
		$data = [
			'nom' => $this->faker->name,
			'email' => $this->faker->email,
			'login' => $this->faker->userName,
			'statut' => '1comite',
			'lang' => $this->lang,
		];
		if ($this->faker->numberBetween(0, 100) < 40) {
			$data['bio'] = $this->generateParagraph();
		}
		if ($this->faker->numberBetween(0, 100) < 10) {
			$data['pgp'] = $this->generateParagraph();
		}
		if ($this->faker->numberBetween(0, 100) < 10) {
			$data['nom_site'] = $this->generateTitle();
			$data['url_site'] = $this->faker->url;
		}

		autoriser_exception('creer', 'auteur', '', true);
		$id_auteur = objet_inserer('auteur', null, $data);
		autoriser_exception('creer', 'auteur', '', false);

		return $id_auteur;
	}


	/** Générer des articles */
	public function seedArticles($id_rubrique) {
		$articles = [];
		for ($i = 0; $i < $this->count['articles']; $i++) {
			if ($id_article = $this->seedArticle($id_rubrique)) {
				$articles[] = $id_article;
			}
		}
		return $articles;
	}

	/** Générer un article */
	function seedArticle($id_rubrique) {
		$data = [
			'id_rubrique' => $id_rubrique,
			'titre' => $this->generateTitle(),
			'statut' => 'publie',
			'lang' => $this->lang,
			'date' => $this->faker->dateTimeBetween('-3 years')->format('Y-m-d H:i:s')
		];
		$data['date_modif'] = $data['date'];
		if ($this->faker->numberBetween(0, 100) < 30) {
			$data['date_modif'] = $this->faker->dateTimeBetween($data['date'])->format('Y-m-d H:i:s');
		}
		if ($this->faker->numberBetween(0, 100) < 10) {
			$data['surtitre'] = $this->generateTitle();
		}
		if ($this->faker->numberBetween(0, 100) < 10) {
			$data['soustitre'] = $this->generateTitle();
		}
		if ($this->faker->numberBetween(0, 100) < 40) {
			$data['descriptif'] = $this->generateParagraph();
		}
		if ($this->faker->numberBetween(0, 100) < 40) {
			$data['chapo'] = $this->generateParagraph(300, 600);
		}
		if ($this->faker->numberBetween(0, 100) < 90) {
			$data['texte'] = $this->generateText(2, 20);
		}
		if ($this->faker->numberBetween(0, 100) < 10) {
			$data['ps'] = $this->generateParagraph();
		}
		if ($this->faker->numberBetween(0, 100) < 10) {
			$data['nom_site'] = $this->generateTitle();
			$data['url_site'] = $this->faker->url;
		}

		autoriser_exception('creer', 'article', '', true);
		$id_article = objet_inserer('article', $id_rubrique, $data);
		autoriser_exception('creer', 'article', '', false);

		return $id_article;
	}

	/** Tisser des liens */
	public function seedLinks($objets_source, $objets_lies) {
		foreach ($objets_source as $objet_source => $ids_source) {
			foreach ($objets_lies as $objet_lie => $ids_lie) {
				foreach ($ids_lie as $id_lie) {
					$nb = $this->faker->numberBetween(1, 3);
					$ids = $this->faker->randomElements($ids_source, $nb);
					objet_associer([$objet_source => $ids], [$objet_lie => $id_lie]);
				}
			}
		}
	}

	/** Générer un titre (spip) */
	public function generateTitle($min = 30, $max = 100) {
		return $this->faker->realText($this->faker->numberBetween($min, $max));
	}

	/** Générer un paragraphe */
	public function generateParagraph($min = 120, $max = 400) {
		return $this->faker->realText($this->faker->numberBetween($min, $max));
	}

	/** Générer un texte (spip) */
	public function generateText($min = 1, $max = 6) {
		$nb = $this->faker->numberBetween($min, $max);
		$text = '';
		for ($i = 0; $i < $nb; $i++) {
			if ($text) {
				$text .= "\n\n";
			}
			// Intertitres
			if ($this->faker->numberBetween(0, 100) < 10) {
				$text .= $this->generateHeading();
			}
			// Listes
			if ($this->faker->numberBetween(0, 100) < 10) {
				$text .= $this->generateList();
			}
			$text .= $this->generateParagraph(200, 800);
		}
		return $text;
	}

	/** Générer un intertitre */
	public function generateHeading() {
		return "{{{" . $this->generateTitle() . "}}}\n\n";
	}

	/** Générer une liste */
	public function generateList() {
		$j = $this->faker->numberBetween(3, 10);
		$list = "\n";
		for ($i = 0; $i < $j; $i++) {
			$list .= "-* " . $this->generateTitle() . "\n";
		}

		return $list . "\n";
	}

}