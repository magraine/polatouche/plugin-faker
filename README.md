# Polatouche : plugin Faker

Ajoute des commandes à Polatouche afin d’ajouter des faux contenus dans un site SPIP.

## Liste des commandes

### Locales disponibles

Liste des locales utilisables avec Faker

    polatouche faker:locales

Liste des locales utilisables avec Faker ayant des faux textes dans la langue de la locale.
Sans une locale complète, Faker utilisera soit l’anglais, soit du lorem ipsum en fonction de l’expression demandé.

    polatouche faker:locales -w

### Remplir un site SPIP

Créer une rubrique avec quelques articles / auteurs dans une locale.
Le script demandera de saisir la locale souhaitée :

    polatouche faker:seed

Idem en indiquant directement une locale en option.

    polatouche faker:seed --locale=fr_FR

